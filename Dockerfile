# FROM node:carbon
FROM node:carbon

MAINTAINER "IV"

WORKDIR /home/app
RUN chown -R node:node /home/app

COPY package*.json ./
RUN npm install

COPY . .
RUN npm run test
RUN npm run build
CMD [ "node", "/home/app/dist/server.js"]