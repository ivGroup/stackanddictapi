'use strict'

const gulp = require('gulp')

const path = require('path')
const del = require("del")
const nodemon = require('gulp-nodemon')
const babel = require('gulp-babel')

const uglify = require('gulp-uglify')
const Cache = require('gulp-file-cache')
const mocha = require('gulp-mocha')
const eslint = require('gulp-eslint')
const gulpIf = require('gulp-if')

const baseDir = __dirname
const cache = new Cache()

function compileJs() {
    return gulp.src(baseDir + '/src/**/*.js')
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(gulp.dest(baseDir + '/dist'))
}

function compileJsWithCash() {
    return gulp.src(baseDir + '/src/**/*.js')
        .pipe(cache.filter())
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(cache.cache())
        .pipe(gulp.dest(baseDir + '/dist'))
}

function esLint() {
    return gulp.src(baseDir + '/src/**/*.js')
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError())
}

function isFixed(file) {
    return file.eslint !== null && file.eslint.fixed
}

function eslintFix() {
    return gulp.src(baseDir + '/src/**/*.js')
        .pipe(eslint({
            fix: true,
        }))
        .pipe(eslint.format())
        .pipe(gulpIf(isFixed, gulp.dest('./src/')))
        .pipe(eslint.failAfterError())
}

function uglifyJS() {
    return gulp.src(baseDir + '/dist/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest(baseDir + '/dist/'))
}

function mochaTest() {
    const mochaOptions = {
        reporter: 'nyan',
        timeout: 5000,
    }
    return gulp.src('test/*-test.js', { read: false })
        .pipe(mocha(mochaOptions))
}

function clean() {
    return del([baseDir + '/dist'])
}

function watchFiles(done) {
    const stream = nodemon({
        script: 'dist/server.js',
        ext: 'js',
        ignore: ['gulpfile.js', 'node_modules/', 'dist/'],
        tasks: function (changedFiles) {
            var tasks = []
            if (!changedFiles) return tasks;
            changedFiles.forEach(function (file) {
                if (path.extname(file) === '.js' && !~tasks.indexOf('compileJsWithCash'))
                    tasks.push('compileJsWithCash')
            })
            return tasks
        },
        done
    })
    stream
        .on('restart', function () {
            console.log('restarted!')
        })
        .on('crash', function () {
            console.error('Application has crashed!\n')
        })
}

const defaultBuild = gulp.series(clean, eslintFix, compileJs, uglifyJS)
const test = gulp.series(defaultBuild, mochaTest)
const watch = gulp.series(defaultBuild, watchFiles)

exports.eslint = esLint
exports.eslintfix = eslintFix
exports.compileJsWithCash = compileJsWithCash
exports.clean = clean
exports.build = defaultBuild
exports.watch = watch
exports.default = defaultBuild
exports.test = test
