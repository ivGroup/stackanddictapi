'use strict'

process.env.NODE_ENV = 'test'
process.env.PORT = 3000


const server = require('../dist/server')

require('./functional/httpError-test')
require('./functional/stack-test')
require('./functional/store-test')
