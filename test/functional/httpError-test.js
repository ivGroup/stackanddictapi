'use strict'

const chai = require('chai')
const chaiHttp = require('chai-http')

const expect = chai.expect
chai.use(chaiHttp)

const serverAddress = 'http://localhost:' + process.env.PORT || 5000

describe('Testing 404 and internal server error', () => {

    it('/GET from empty directore', (done) => {
        chai.request(serverAddress)
            .get('/emptyDirectory')
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(404)
                expect(text.error.message)
                    .to.equal('Not Found')
                done()
            })
    })
    it('/GET from directory which throw uncatched error', (done) => {
        chai.request(serverAddress)
            .get('/api/v1/internalServerErrorTest')
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(500)
                expect(text.error.message)
                    .to.equal('Internal server error')
                done()
            })
    })
})
