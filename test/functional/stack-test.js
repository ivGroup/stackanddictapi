'use strict'

const chai = require('chai')
const chaiHttp = require('chai-http')

const expect = chai.expect
chai.use(chaiHttp)

const serverAddress = 'http://localhost:' + process.env.PORT || 5000

describe('Testing stack LIFO functional', () => {
    const apiURL = '/api/v1/stack'

    it('/GET from empty stack', (done) => {
        chai.request(serverAddress)
            .get(apiURL)
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(404)
                expect(text.error.message)
                    .to.equal('Stack is empty')
                done()
            })
    })

    it('/POST Hello to stack', (done) => {
        chai.request(serverAddress)
            .post(apiURL)
            .query({ value: 'Hello' })
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(200)
                expect(text.data.description)
                    .to.equal('successful operation')
                done()
            })
    })

    it('/POST World to stack', (done) => {
        chai.request(serverAddress)
            .post(apiURL)
            .query({ value: 'World' })
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(200)
                expect(text.data.description)
                    .to.equal('successful operation')
                done()
            })
    })

    it('/GET World from stack', (done) => {
        chai.request(serverAddress)
            .get(apiURL)
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(200)
                expect(text.data)
                    .to.equal('World')
                done()
            })
    })

    it('/GET Hello from stack', (done) => {
        chai.request(serverAddress)
            .get(apiURL)
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(200)
                expect(text.data)
                    .to.equal('Hello')
                done()
            })
    })

    it('/GET from empty stack in the end', (done) => {
        chai.request(serverAddress)
            .get(apiURL)
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(404)
                expect(text.error.message)
                    .to.equal('Stack is empty')
                done()
            })
    })
})

describe('Testing stack API params validation', () => {
    const apiURL = '/api/v1/stack'

    it('/POST to stack with NO value', (done) => {
        chai.request(serverAddress)
            .post(apiURL)
            .query()
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(400)
                expect(text.error.message)
                    .to.equal('Invalid input')
                done()
            })
    })

    it('/POST to stack with empty value', (done) => {
        chai.request(serverAddress)
            .post(apiURL)
            .query({ value: '' })
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(400)
                expect(text.error.message)
                    .to.equal('Invalid input')
                done()
            })
    })

    it('/POST to stack with normal value', (done) => {
        chai.request(serverAddress)
            .post(apiURL)
            .query({ value: '12ff' })
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(200)
                expect(text.data.description)
                    .to.equal('successful operation')
                done()
            })
    })
})
