'use strict'

const chai = require('chai')
const chaiHttp = require('chai-http')

const expect = chai.expect
chai.use(chaiHttp)

const serverAddress = 'http://localhost:' + process.env.PORT || 5000

describe('Testing store key-value functional', () => {
    const apiURL = '/api/v1/store'

    it('/GET undefined key', (done) => {
        chai.request(serverAddress)
            .get(apiURL)
            .query({ key: 'name' })
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(404)
                expect(text.error.message)
                    .to.equal('Key is not exist')
                done()
            })
    })

    it('/POST key-value pair', (done) => {
        chai.request(serverAddress)
            .post(apiURL)
            .query({ key: 'name', value: 'Ivan' })
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(200)
                expect(text.data.value)
                    .to.equal('Ivan')
                done()
            })
    })

    it('/GET existing key', (done) => {
        chai.request(serverAddress)
            .get(apiURL)
            .query({ key: 'name' })
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(200)
                expect(text.data.value)
                    .to.equal('Ivan')
                done()
            })
    })

    it('/GET existing key second time', (done) => {
        chai.request(serverAddress)
            .get(apiURL)
            .query({ key: 'name' })
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(200)
                expect(text.data.value)
                    .to.equal('Ivan')
                done()
            })
    })

    it('/DELETE existing key', (done) => {
        chai.request(serverAddress)
            .delete(apiURL)
            .query({ key: 'name' })
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(200)
                expect(text.data.description)
                    .to.equal('successful operation')
                done()
            })
    })

    it('/GET deleted key', (done) => {
        chai.request(serverAddress)
            .get(apiURL)
            .query({ key: 'name' })
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(404)
                expect(text.error.message)
                    .to.equal('Key is not exist')
                done()
            })
    })
})


describe('Testing store api params validation', () => {
    const apiURL = '/api/v1/store'

    it('/GET with NO key', (done) => {
        chai.request(serverAddress)
            .get(apiURL)
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(400)
                expect(text.error.message)
                    .to.equal('Invalid input')
                done()
            })
    })

    it('/GET with empty key', (done) => {
        chai.request(serverAddress)
            .get(apiURL)
            .query({ key: '' })
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(400)
                expect(text.error.message)
                    .to.equal('Invalid input')
                done()
            })
    })

    it('/POST with NO key', (done) => {
        chai.request(serverAddress)
            .post(apiURL)
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(400)
                expect(text.error.message)
                    .to.equal('Invalid input')
                done()
            })
    })

    it('/POST with empty key', (done) => {
        chai.request(serverAddress)
            .post(apiURL)
            .query({ key: '' })
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(400)
                expect(text.error.message)
                    .to.equal('Invalid input')
                done()
            })
    })

    it('/POST with NO value', (done) => {
        chai.request(serverAddress)
            .post(apiURL)
            .query({ key: 'name' })
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(400)
                expect(text.error.message)
                    .to.equal('Invalid input')
                done()
            })
    })

    it('/POST with empty value', (done) => {
        chai.request(serverAddress)
            .post(apiURL)
            .query({ key: 'name', value: '' })
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(400)
                expect(text.error.message)
                    .to.equal('Invalid input')
                done()
            })
    })

    it('/POST with empty ttl', (done) => {
        chai.request(serverAddress)
            .post(apiURL)
            .query({ key: 'd', value: 'a', ttl: '' })
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(400)
                expect(text.error.message)
                    .to.equal('Invalid input')
                done()
            })
    })

    it('/POST with unvalid ttl', (done) => {
        chai.request(serverAddress)
            .post(apiURL)
            .query({ key: 'd', value: 'a', ttl: 'adfs' })
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(400)
                expect(text.error.message)
                    .to.equal('Invalid input')
                done()
            })
    })

    it('/DELETE with NO key', (done) => {
        chai.request(serverAddress)
            .delete(apiURL)
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(400)
                expect(text.error.message)
                    .to.equal('Invalid input')
                done()
            })
    })

    it('/DELETE with empty key', (done) => {
        chai.request(serverAddress)
            .delete(apiURL)
            .query({ key: '' })
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(400)
                expect(text.error.message)
                    .to.equal('Invalid input')
                done()
            })
    })
})

describe('Testing store TTL functional', () => {
    const apiURL = '/api/v1/store'

    it('/POST key with ttl', (done) => {
        chai.request(serverAddress)
            .post(apiURL)
            .query({ key: 'name', value: 'Ivan', ttl: '2' })
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(200)
                expect(text.data.value)
                    .to.equal('Ivan')
                done()
            })
    })

    it('/GET existing key imidiately', (done) => {
        chai.request(serverAddress)
            .get(apiURL)
            .query({ key: 'name' })
            .end((err, res) => {
                const text = JSON.parse(res.text)
                expect(res)
                    .to.have.status(200)
                expect(text.data.value)
                    .to.equal('Ivan')
                done()
            })
    })

    it('/GET existing key after ttl', (done) => {
        setTimeout(() => {
            chai.request(serverAddress)
                .get(apiURL)
                .query({ key: 'name' })
                .end((err, res) => {
                    const text = JSON.parse(res.text)
                    expect(res)
                        .to.have.status(404)
                    expect(text.error.message)
                        .to.equal('Key is not exist')
                    done()
                })
        }, 3000)
    })

})
