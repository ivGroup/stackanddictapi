import { validationResult, query } from 'express-validator/check';

import logger from './logger';

export const validate = {};

validate.addToStack = [
  query('value', 'value doesn`t exist')
    .exists()
    .not()
    .isEmpty(),
];

validate.getFromStack = [];

validate.setInStore = [
  query('key', 'key doesn`t exist')
    .exists()
    .not()
    .isEmpty(),
  query('value', 'value doesn`t exist')
    .exists()
    .not()
    .isEmpty(),
  query('ttl', 'ttl has unproper format')
    .optional()
    .isInt({ min: 0 }),
];

validate.getFromStore = [
  query('key', 'key doesn`t exist')
    .exists()
    .not()
    .isEmpty(),
];

validate.deleteFromStore = [
  query('key', 'key doesn`t exist')
    .exists()
    .not()
    .isEmpty(),
];


export const sendSuccess = (res, data) => {
  const responceObject = data || { description: 'successful operation' };
  res
    .status(200)
    .json({
      status: true,
      data: responceObject,
    });
};

export const validationHandler = (req, res, next) => {
  const validationError = validationResult(req);

  if (!validationError.isEmpty()) {
    logger.debug(validationError.array());

    const httpError = new Error('Invalid input');
    httpError.status = 400;
    throw httpError;
  }
  return next();
};
