

const appConfig = {};

appConfig.port = process.env.PORT || 5000;
appConfig.status = process.env.NODE_ENV || 'dev';

appConfig.enableWorker = process.env.ENABLE_WORKER || 'true';

export default appConfig;
