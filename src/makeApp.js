import express from 'express';
import expressValidator from 'express-validator';
import morgan from 'morgan';

import swaggerUi from 'swagger-ui-express';
import YAML from 'yamljs';
import appConfig from './config';
import logger from './models/logger';
import mainRouter from './mainRouter';


const swaggerDocument = YAML.load(__dirname.replace('dist', 'swagger-2.yaml'));

const options = {
  swaggerOptions: {
    validatorUrl: null,
  },
};

export const app = express();

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Methods', 'GET, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Origin,X-Requested-With,Content-Type,Accept,content-type,application/json');
  if (req.method === 'OPTIONS') {
    return res.status(200)
      .json({});
  }
  return next();
});

app.use(express.json());
app.use(expressValidator());

if (appConfig.status === 'dev') {
  app.use(morgan('dev'));
} else {
  app.use(morgan('tiny'));
}

app.use('/api/v1', mainRouter);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, options));

app.use((req, res, next) => {
  const newError = new Error('Not Found');
  newError.status = 404;
  next(newError);
});

app.use((err, req, res, next) => {
  const status = err.status || 500;
  res.status(status);
  res.json({
    status: false,
    error: {
      message: status === 500 ? 'Internal server error' : err.message,
    },
  });
});

export const startApp = async (port) => {
  const appPort = port || appConfig.port;
  app.listen(appPort, () => {
    logger.debug(`Web server listening on: ${appPort}`);
    if (appConfig.status === 'dev') {
      logger.debug(`Click to visit it here ->: http://localhost:${appPort}/api/v1`);
      logger.debug(`Click to visit api here ->: http://localhost:${appPort}/api-docs`);
    }
  });
};
