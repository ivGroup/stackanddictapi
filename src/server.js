import 'babel-polyfill';

import logger from './models/logger';

import { startApp } from './makeApp';
import appConfig from './config';

import Worker from './models/worker';
import store from './models/store';

startApp()
  .catch((err) => {
    logger.error(`Error in express middelware: ${err}`);
  });

if (appConfig.enableWorker === 'true') {
  const deleteTTlWorker = new Worker('*/1 * * * *', async () => {
    const nowInMills = +new Date();
    Object.keys(store)
      .forEach((key) => {
        if (store[key].ttl < nowInMills) {
          delete store[key];
        }
      });
  });
  deleteTTlWorker.startJob();
}
