import { Router } from 'express';
import appConfig from './config';

import stackRouter from './components/stack/stackRouter';

import storeRouter from './components/store/storeRouter';

const mainRouter = Router();

mainRouter.use('/stack', stackRouter);

mainRouter.use('/store', storeRouter);

if (appConfig.status === 'test') {
  mainRouter.get('/internalServerErrorTest', () => {
    throw new Error('Secret coding information which shouldn\'t be revealed');
  });
}

export default mainRouter;
