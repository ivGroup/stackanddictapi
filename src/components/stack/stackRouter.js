import { Router } from 'express';

import { getFromStack, addToStack } from './stackController';
import { validate, validationHandler } from '../../models/validator';

const stackRouter = Router();

stackRouter.get('/',
  validate.getFromStack,
  validationHandler,
  getFromStack);

stackRouter.post('/',
  validate.addToStack,
  validationHandler,
  addToStack);

export default stackRouter;
