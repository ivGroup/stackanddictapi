import stack from '../../models/stack';
import { sendSuccess } from '../../models/validator';

export const addToStack = (req, res) => {
  const { value } = req.query;

  stack.push(value);
  return sendSuccess(res);
};

export const getFromStack = (req, res) => {
  const topValue = stack.pop();
  if (topValue === undefined) {
    const notFoundError = new Error('Stack is empty');
    notFoundError.status = 404;
    throw notFoundError;
  }
  return sendSuccess(res, topValue);
};
