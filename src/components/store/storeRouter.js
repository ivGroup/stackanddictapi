import { Router } from 'express';

import { getFromStore, setInStore, deleteFromStore } from './storeController';
import { validate, validationHandler } from '../../models/validator';

const storeRouter = Router();

storeRouter.get('/',
  validate.getFromStore,
  validationHandler,
  getFromStore);

storeRouter.post('/',
  validate.setInStore,
  validationHandler,
  setInStore);

storeRouter.delete('/',
  validate.deleteFromStore,
  validationHandler,
  deleteFromStore);

export default storeRouter;
