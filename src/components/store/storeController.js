

import store from '../../models/store';
import { sendSuccess } from '../../models/validator';

export const setInStore = (req, res) => {
  const { key, value, ttl } = req.query;

  const nowInMills = +new Date();
  store[key] = { value, ttl: ttl * 1000 + nowInMills };
  return sendSuccess(res, store[key]);
};

export const getFromStore = (req, res) => {
  const { key } = req.query;

  const nowInMills = +new Date();
  if (store[key] && store[key].ttl < nowInMills) delete store[key];

  if (!store[key]) {
    const notFoundError = new Error('Key is not exist');
    notFoundError.status = 404;
    throw notFoundError;
  }
  return sendSuccess(res, store[key]);
};

export const deleteFromStore = (req, res) => {
  const { key } = req.query;
  if (!store[key]) {
    const notFoundError = new Error('Key is not exist');
    notFoundError.status = 404;
    throw notFoundError;
  }
  delete store[key];
  return sendSuccess(res);
};
