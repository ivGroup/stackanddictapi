# Stack and Key-Value store API aplication

## Installation

1. Clone the existing project: `git clone https://github.com/ivanvolov/stackAndDict.git`
2. Enter folder: `cd stackAndDict`
3. Install dependencies: `npm install`
3. Test aplication: `npm test`
4. Start application: `npm start --production`
5. Visit [http://127.0.0.1:5000/api-docs](http://127.0.0.1:5000/api-docs) in your browser

### Docker

The easiest way to get up and running is using Docker. Once the Docker CLI is installed from [https://www.docker.com/get-docker](https://www.docker.com/get-docker).

1. Enter the root of the stackAndDict application
2. Run: `docker-compose up --build`
4. Visit [http://127.0.0.1/api-docs](http://127.0.0.1/api-docs) in your browser
